package iut.tdd;

public class RomanNumerals {
	public String convertToRoman(String arabe) {
		int nb = Integer.parseInt(arabe);
		int combienDeFois;
		int diviseur = 1000;
		int cpt = 0;
		String s = "";
		
		while(nb !=0){
			combienDeFois = nb / diviseur;
			
			for (int i = 0;i<combienDeFois;i++){
				if (combienDeFois == 4){
					s = s + conv(diviseur) + conv(diviseur*5);
					nb = nb - 4*diviseur;
					combienDeFois = 0;
				}else{
					s = s + conv(diviseur);
					nb = nb - diviseur;
				}
			}

			if (nb == 9){
				s+="IX";
				nb -= 9;
			}
			if (cpt%2 == 0) {
				diviseur /= 2;
				cpt++;
			} else {
				diviseur /= 5;
				cpt++;
			}
			
		}
		
	
		return s;
	}

	
	

	private String conv(int i) {
		if (i == 1000) 
			return "M";
		if (i == 500) 
			return "D";
		if (i == 100) 
			return "C";
		if (i == 50) 
			return "L";
		if (i == 10) 
			return "X";
		if (i == 5) 
			return "V";
		else 
			return "I";

	}




	public String convertFromRoman(String roman) {
		return "1";
	}

}
