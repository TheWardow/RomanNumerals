package iut.tdd;

import org.junit.Assert;
import org.junit.Test;

public class RomanNumeralsTest {
	@Test
	public void should_return_I_when_1 () {
		testConvertToRoman("1", "I");
	}
	@Test
	public void should_return_II_when_2 () {
		testConvertToRoman("2", "II");
	}
	@Test
	public void should_return_IV_when_4 () {
		testConvertToRoman("4", "IV");
	}
	@Test
	public void should_return_XII_when_12 () {
		testConvertToRoman("12", "XII");
	}
	@Test
	public void should_return_XV_when_15 () {
		testConvertToRoman("15", "XV");
	}
	@Test
	public void should_return_LXXVI_when_76 () {
		testConvertToRoman("76", "LXXVI");
	}
	@Test
	public void should_return_CLII_when_152 () {
		testConvertToRoman("152", "CLII");
	}
	
	@Test
	public void should_return_CDXLIV_when_444 () {
		testConvertToRoman("444", "CDXLIV");
	}
	@Test
	public void should_return_IX_when_9 () {
		testConvertToRoman("9", "IX");
	}
	
	@Test
	public void should_return_MMMCDXLIX_when_3449 () {
		testConvertToRoman("3449", "MMMCDXLIX");
	}
	
	private void testConvertToRoman(String input, String expected) {
		RomanNumerals romanNumeral = new RomanNumerals();
		//When
		String actual = romanNumeral.convertToRoman(input);
		//Then
		Assert.assertEquals(expected, actual);
	}
}
